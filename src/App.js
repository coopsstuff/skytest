import * as actionCreators from './actions/productActions'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import Main from './components/Main'
import './App.css'

function mapStateToProps(state) {
  return {config: state.config}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actionCreators, dispatch)
}

const App = connect(mapStateToProps, mapDispatchToProps)(Main)

export default App
