export const toggleArsenalTVCheckbox = value => {
  return { type: 'SPORTS_PRODUCT_ARSENAL_CHECKBOX', value }
}

export const toggleChelseaTVCheckbox = value => {
  return { type: 'SPORTS_PRODUCT_CHELSEA_CHECKBOX', value }
}

export const toggleLiverpoolTVCheckbox = value => {
  return { type: 'SPORTS_PRODUCT_LIVERPOOL_CHECKBOX', value }
}

export const toggleSkyNewsCheckbox = value => {
  return { type: 'NEWS_PRODUCT_SKYNEWS_CHECKBOX', value }
}

export const toggleSkySportsNewsCheckbox = value => {
  return { type: 'NEWS_PRODUCT_SKYSPORTS_CHECKBOX', value }
}

export const updateUserLocation = value => {
  return { type: 'USER_SELECT', value }
}


//Toggle Selection state
export const updateCustomer = value => {
  return dispatch =>{
    dispatch(updateUserLocation(value))
  }
}

export const updateArsenalToggle = value => {
  return dispatch =>{
    dispatch(toggleArsenalTVCheckbox(value))
  }
}

export const updateChelseaToggle = value => {
  return dispatch =>{
    dispatch(toggleChelseaTVCheckbox(value))
  }
}

export const updateLiverpoolToggle = value => {
  return dispatch =>{
    dispatch(toggleLiverpoolTVCheckbox(value))
  }
}

export const updateSkyNewsToggle = value => {
  return dispatch =>{
    dispatch(toggleSkyNewsCheckbox(value))
  }
}

export const updateSkySportsNewsToggle = value => {
  return dispatch =>{
    dispatch(toggleSkySportsNewsCheckbox(value))
  }
}
