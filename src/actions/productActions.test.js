/*global jest describe it expect beforeEach */
import configureMockStore from 'redux-mock-store'
import * as actions from './productActions'
import thunk from 'redux-thunk'

const middleWares = [thunk] //required for dispatches
const mockStore = configureMockStore(middleWares)

let store = ''

beforeEach(() => {
    store = mockStore({})
})

describe('updateCustomer()', () => {
    it('dispatches USER_SELECT on a request to update the user state', () => {
        const expectedActions = [
            { type: 'USER_SELECT', value: 'London'}
        ]
        store.dispatch(actions.updateCustomer('London'))
        expect(store.getActions()).toEqual(expectedActions)
    })
})
describe('updateArsenalToggle()', () => {
    it('dispatches SPORTS_PRODUCT_ARSENAL_CHECKBOX on a request to update the Arsenal TV Channel state', () => {
        const expectedActions = [
            { type: 'SPORTS_PRODUCT_ARSENAL_CHECKBOX', value: true}
        ]
        store.dispatch(actions.updateArsenalToggle(true))
        expect(store.getActions()).toEqual(expectedActions)
    })
})
describe('updateChelseaToggle()', () => {
    it('dispatches SPORTS_PRODUCT_CHELSEA_CHECKBOX on a request to update the Chelsea TV Channel state', () => {
        const expectedActions = [
            { type: 'SPORTS_PRODUCT_CHELSEA_CHECKBOX', value: true}
        ]
        store.dispatch(actions.updateChelseaToggle(true))
        expect(store.getActions()).toEqual(expectedActions)
    })
})
describe('updateLiverpoolToggle()', () => {
    it('dispatches SPORTS_PRODUCT_LIVERPOOL_CHECKBOX on a request to update the Liverpool TV Channel state', () => {
        const expectedActions = [
            { type: 'SPORTS_PRODUCT_LIVERPOOL_CHECKBOX', value: true}
        ]
        store.dispatch(actions.updateLiverpoolToggle(true))
        expect(store.getActions()).toEqual(expectedActions)
    })
})
describe('updateSkyNewsToggle()', () => {
    it('dispatches NEWS_PRODUCT_SKYNEWS_CHECKBOX on a request to update the Sky News Channel state', () => {
        const expectedActions = [
            { type: 'NEWS_PRODUCT_SKYNEWS_CHECKBOX', value: true}
        ]
        store.dispatch(actions.updateSkyNewsToggle(true))
        expect(store.getActions()).toEqual(expectedActions)
    })
})
describe('updateSkySportsNewsToggle()', () => {
    it('dispatches NEWS_PRODUCT_SKYSPORTS_CHECKBOX on a request to update the Sky Sports News Channel state', () => {
        const expectedActions = [
            { type: 'NEWS_PRODUCT_SKYSPORTS_CHECKBOX', value: true}
        ]
        store.dispatch(actions.updateSkySportsNewsToggle(true))
        expect(store.getActions()).toEqual(expectedActions)
    })
})