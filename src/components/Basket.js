import React, { Component } from 'react'
import '../shared/styles/styles.css'

export default class Basket extends Component {
  render() {

    const { 
      arsenalTVSelected, 
      chelseaTVSelected, 
      liverpoolTVSelected,
      skyNewsSelected, 
      skySportsNewsSelected,
      basketContents } = this.props.config

    const noItemsSelected = basketContents === 
    undefined || basketContents.length === 0

    return (
      <div className="gridItem basket">
        <span className="heading basketHeading">Your Basket</span>
        <div className="basketProductContainer">
          {noItemsSelected && <div className="basketItem">
              No Channels Selected
            </div>}
          {arsenalTVSelected && <div className="basketItem">
              - Arsenal TV
            </div>
          }
          {chelseaTVSelected && <div className="basketItem">
              - Chelsea TV
            </div>
          }
          {liverpoolTVSelected && <div className="basketItem">
              - Liverpool TV
            </div>
          }
          {skyNewsSelected && <div className="basketItem">
              - Sky News
            </div>
          }
          {skySportsNewsSelected && <div className="basketItem">
              - Sky Sports News
            </div>
          }
        </div>

        {/* Button disabled if basket empty */}
        <div className="basketButtonContainer">
          <button className="basketButton" type="button" disabled={noItemsSelected}>Checkout</button>
        </div>
      </div>
    );
  }
}