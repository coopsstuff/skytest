import React, { Component } from 'react';
import Sports from './Sports'
import News from './News'
import Basket from './Basket'
import User from './UserSelect'

export default class Main extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <div className="sky-logo">
            <div class="hide">Sky</div>
          </div>
          header content
        </header>
        <div class="divide">Select your TV bundle</div>
        <div className="productGrid">
            <Sports {...this.props}  />
            <News {...this.props}  />
            <Basket {...this.props}  />
            <User {...this.props} />
        </div>
      </div>
    );
  }
}
