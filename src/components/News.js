import React, { Component } from 'react'
import newsData from '../services/news.json'
import Checkbox from '../shared/Checkbox'
import '../shared/styles/styles.css'

export default class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedItems: new Map(),
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e) {
    const news = e.target.name
    const isChecked = e.target.checked
    this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(news, isChecked) }))
    let basket = this.props.config.basketContents
    isChecked ? basket.push(news) : basket = basket.splice( basket.indexOf(news), 1 )
    news==="Sky News" 
      ? this.props.updateSkyNewsToggle(isChecked)
      : news==="Sky Sports News" 
      && this.props.updateSkySportsNewsToggle(isChecked)
  }

  render() {
    return (
      <div className="gridItem">
      <span className="heading newsHeading">News Channels</span>
        {newsData.map(news =>
            <div key={news.id} className="checkItem">
                <label>
                  <span className="gridItemText">{news.name}</span>
                  <Checkbox name={news.name} checked={this.state.checkedItems.get(news.name)} onChange={this.handleChange} />
                </label>
            </div>
          )
        }
        </div>
    );
  }
}