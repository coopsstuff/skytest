import React, { Component } from 'react'
import sportsData from '../services/sports.json'
import Checkbox from '../shared/Checkbox'
import '../shared/styles/styles.css'

export default class Sports extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checkedItems: new Map(),
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e) {
    const product = e.target.name;
    const isChecked = e.target.checked;
    let basket = this.props.config.basketContents
    product==="Arsenal TV"
      ? this.props.updateArsenalToggle(isChecked)
      : product==="Liverpool TV"
      ? this.props.updateLiverpoolToggle(isChecked) :
      product==="Chelsea TV" &&
      this.props.updateChelseaToggle(isChecked)
    isChecked ? basket.push(product) : basket = basket.splice( basket.indexOf(product), 1 )
    this.setState(prevState => ({ checkedItems: prevState.checkedItems.set(product, isChecked) }))
  }  
  
  render() {
    const userLocation = this.props.config.userLocation
    const locationFilteredProducts =  sportsData.filter(function(product) {
      return product.location === userLocation;
    });
    return (
      <div className="gridItem">
      <span className="heading sportsHeading">Sports Channels</span>
        {locationFilteredProducts.map(product =>
          <div key={product.id} className="checkItem">
            <label>
              <span className="gridItemText">{product.name}</span>
              <Checkbox name={product.name} checked={this.state.checkedItems.get(product.name)} onChange={this.handleChange} />
            </label>
          </div>
          )
        }
        </div>
    );
  }
}