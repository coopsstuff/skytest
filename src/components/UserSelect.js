import React, { Component } from 'react'
import userData from '../services/customers.json'
import Checkbox from '../shared/Checkbox'
import '../shared/styles/styles.css'

//Manual user switch so we can prove the location dependencies.

export default class Sports extends Component {
  constructor(props) {
    super(props)
    this.state = {
      checkedItems: new Map(),
    }
    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(e) {
    const location = e.target.name;
    this.props.updateCustomer(location)
    //For the sake of this exercise i'm removing the possibility of being able
    //to select a TV channel from the wrong area when you switch between users! :)
    this.props.updateArsenalToggle(false)
    this.props.updateLiverpoolToggle(false)
    this.props.updateChelseaToggle(false)
  }  
  
  render() {
    return (
      <div className="userSelect">
        {userData.map(user =>
          <div key={user.customerId} className="checkItem">
            <label>
            <span className="gridItemText">{user.customerId} - {user.name} - {user.locationId}</span>
              <Checkbox name={user.locationId} onChange={this.handleChange} />
            </label>
          </div>
          )
        }
        </div>
    );
  }
}