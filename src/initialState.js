export default {
    config: {
        //I initially considered pushing each selected product to an array 
        //named 'basketContents' to fulfill the basket area 
        //but decided that it was quicker for me to manage each 
        //product seperately from a visual perspective for this example.  
        //I have left this in to facilitate my empty basket check though.
        basketContents: [],
        arsenalTVSelected: false,
        chelseaTVSelected: false,
        liverpoolTVSelected: false,
        skyNewsSelected: false,
        skySportsNewsSelected: false,
        //I considered a couple of ways of handling the user location.  
        //Going on the assumption that a user's details 
        //is injected in to the intitial state perhaps from the window or otherwise
        //I am running with the location being updated in the state manually via
        //a redux action from a trigger in the UI.
        userLocation: 'London'
    }   
}