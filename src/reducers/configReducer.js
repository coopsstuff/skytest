import initialState from '../initialState'

export default (state, action) => {
    switch (action.type) {
        case 'USER_SELECT':
            return { ...state, userLocation: action.value}
        case 'SPORTS_PRODUCT_ARSENAL_CHECKBOX':
            return { ...state, arsenalTVSelected: action.value}
        case 'SPORTS_PRODUCT_CHELSEA_CHECKBOX':
            return { ...state, chelseaTVSelected: action.value}
        case 'SPORTS_PRODUCT_LIVERPOOL_CHECKBOX':
            return { ...state, liverpoolTVSelected: action.value}
        case 'NEWS_PRODUCT_SKYNEWS_CHECKBOX':
            return { ...state, skyNewsSelected: action.value}
        case 'NEWS_PRODUCT_SKYSPORTS_CHECKBOX':
            return { ...state, skySportsNewsSelected: action.value}
        default:
        return state || initialState.config
    }
}